import { Injectable } from '@angular/core';
import { retry } from 'rxjs/operators';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private configUrl: string;
  private readonly repeatNumberAfterError:number = 3;

  constructor(private http: HttpService) { }

  getConfig(){
    return this.http.get(this.configUrl).pipe(retry(this.repeatNumberAfterError));
  }
}
