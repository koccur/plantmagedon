import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LoaderService } from '../../loader/loader.service';

@Injectable({
  providedIn: 'root'
})
export class LoadingInterceptor implements HttpInterceptor{

  private activeRequest:number=0;

  constructor(private loaderService: LoaderService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(this.activeRequest === 0){
      this.loaderService.show();
    }
    this.activeRequest++;

    return next.handle(req).pipe(finalize(()=>{
      this.activeRequest--;
      if(this.activeRequest === 0){
        this.loaderService.hide();
      }
    }));
  }


}
