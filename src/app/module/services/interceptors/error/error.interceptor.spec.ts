import { TestBed } from '@angular/core/testing';

import { ErrorInterceptor } from './loading.interceptor';

describe('LoadingService', () => {
  let service: ErrorInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErrorInterceptor);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
