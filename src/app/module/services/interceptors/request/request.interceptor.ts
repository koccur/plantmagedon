import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestInterceptor implements HttpInterceptor{

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const loggedUser = this.getLoggedUser();
    const authReq = req.clone({
      headers: new HttpHeaders({
        'Content-Type':'application/json',
        'Authorization':`Bearer ${loggedUser.token}`
      })
    })
    return undefined;
  }

  private getLoggedUser() {
    return {token:null}
  }
}
