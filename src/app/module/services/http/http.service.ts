import { HttpBackend, HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private httpWithoutInterceptor: HttpClient = new HttpClient(this.httpBackend);

  constructor(private http: HttpClient,
              private httpBackend: HttpBackend) {
  }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(path, { params }).pipe(catchError(this.formatErrors))
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(path, JSON.stringify(body)).pipe(catchError(this.formatErrors))
  }

  post(path: string, body: Object = {}, options: Object={}): Observable<any> {
    return this.http.post(path, JSON.stringify(body)).pipe(catchError(this.formatErrors))
  }

  delete(path: string): Observable<any> {
    return this.http.delete(path).pipe(catchError(this.formatErrors))
  }

  _get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.httpWithoutInterceptor.get(path, { params }).pipe(catchError(this.formatErrors))
  }

  _put(path: string, body: Object = {}): Observable<any> {
    return this.httpWithoutInterceptor.put(path, JSON.stringify(body)).pipe(catchError(this.formatErrors))
  }

  _post(path: string, body: Object = {}, options: Object={}): Observable<any> {
    return this.httpWithoutInterceptor.post(path, JSON.stringify(body)).pipe(catchError(this.formatErrors))
  }

  _delete(path: string): Observable<any> {
    return this.httpWithoutInterceptor.delete(path).pipe(catchError(this.formatErrors))
  }

  private formatErrors(error: any) {
    return throwError(error.error);
  }
}
