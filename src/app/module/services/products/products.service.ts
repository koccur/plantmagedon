import { Injectable } from '@angular/core';
import { Cacheable } from 'ngx-cacheable';
import { ConfigService } from '../config/config.service';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private config;

  constructor(private http: HttpService,
              private configService: ConfigService) {
    this.configService.getConfig().subscribe(res => this.config = res);
  }

  // todo doczytać
  @Cacheable()
  getProducts() {
    return this.http._get('null');
    // return this.http._get(this.config.productConfiguration);
  }
}
